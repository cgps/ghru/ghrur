library(tidyverse)
library(magrittr)
library(GrpString)
#' get_ncbi_amr_metadata
#'
#' function to get NCBI metadata
#' @param selection_columns an optional vector of columns to select from the NCBI metadata
#' @return a data frame with acquired gene metadata from NCBI
get_ncbi_amr_metadata <- function(selection_columns = NULL){
  # Get data about NCBI AMR gene
  ncbi_amr_metadata <- readr::read_tsv("https://ftp.ncbi.nlm.nih.gov/pathogen/Antimicrobial_resistance/Data/latest/ReferenceGeneCatalog.txt", col_types = cols(.default = "c"))
  if (! is.null(selection_columns)){
    ncbi_amr_metadata <- ncbi_amr_metadata %>%
      select(all_of(selection_columns))
  }
  return(ncbi_amr_metadata)
}

#' memoised function to get NCBI metadata
memoised_get_ncbi_amr_metadata <- memoise(get_ncbi_amr_metadata, cache = cachem::cache_mem(max_age = 60 * 60))
#' get_point_mutation_metadata
#'
#' function to get pointfinder metadata
#' @param species one of campylobacter, enterococcus_faecalis, enterococcus_faecium, escherichia_coli, helicobacter_pylori,
#' klebsiella, mycobacterium_tuberculosis, neisseria_gonorrhoeae, salmonella
#' @return a data frame with point mutation metadata
get_point_mutation_metadata <- function(species){
  # Get data about point muattions for the speaces
  point_mutation_metadata <- readr::read_tsv(paste0("https://gitlab.com/cgps/ghru/pipelines/data_sources/ariba_amr_databases/-/raw/2019-10-30/pointfinder/", species, "_db/01.filter.check_metadata.tsv"), col_names = FALSE)
  colnames(point_mutation_metadata) <- c("sequence_name", "gene?", "variant?", "mutation", "gene", "resistance")
  point_mutation_metadata %<>% dplyr::mutate(resistance = stringr::str_match(resistance, "^Resistance: (.+?)\\.")[,2])  %>%
    tidyr::unite(mutation, c(gene,mutation), sep = "_") %>%
    dplyr::select(mutation, resistance)
  return(point_mutation_metadata)
}

#' wide_amr_data_to_long
#'
#' function to get convert acquired gene amr data into long format
#' @param amr_data acquired_amr_data obtained using the ghruR::get_data_for_country(country_value = "<COUNTRY>", type_value = "AMR <SPECIES>", AMR_type = "acquired")
#' @return long_amr_data wide data pivoted to long format with one observation per ariba cluster
wide_amr_data_to_long <- function(amr_data){
  long_amr_data <- amr_data %>%
    dplyr::select(-`Alternative sample id`) %>%
    tidyr::pivot_longer(-`Sample id`, names_to = 'metric', values_to = 'value') %>%
    tidyr::extract(col = 'metric', into = c('ariba_cluster', 'metric'), regex = '^([^\\.]+)\\.(.+)', remove = TRUE)%>%
    tidyr::pivot_wider(names_from = 'metric', values_from = 'value')
  return(long_amr_data)
}

#' filter_long_data
#'
#' function to  filter long format data and return rows
#' @param long_amr_data generated using the ghruR::wide_amr_data_to_long function
#' @param assembled_starts_with the string that the assembled value must start with (default is 'yes')
#' @param pct_id_cutoff the lowest pct_id match (default 90)
#' @param ctg_cov_cutoff the lowest coverage permitted (default 5)
#' @return filtered_long_amr_data a dataframe containing the filtered long amr data
filter_long_data <- function(long_amr_data, assembled_starts_with = "yes", pct_id_cutoff = 90, ctg_cov_cutoff = 5){
  filtered_long_amr_data <- long_amr_data %>%
    filter(str_starts(assembled, assembled_starts_with)) %>%
    mutate(pct_id = as.numeric(pct_id)) %>%
    filter(pct_id >= pct_id_cutoff) %>%
    mutate(ctg_cov = as.numeric(ctg_cov)) %>%
    filter(ctg_cov >= ctg_cov_cutoff)

  return(filtered_long_amr_data)
}

#' extract_nucleotide_accession
#'
#' extract nucleotide accesion and rename to refseq_nucleotide_accession
#' @param long_amr_data generated using the ghruR::wide_amr_data_to_long function and optionally filtered
#' @return long_amr_data_with_accession A dataframe with the accession number extracted as a column called refseq_nucleotide_accession
extract_nucleotide_accession <- function(long_amr_data){
 long_amr_data_with_accession <- long_amr_data %>%
    mutate(ref_seq = stringr::str_extract(ref_seq, "NG_.+$")) %>%
    rename(refseq_nucleotide_accession = ref_seq)

  return(long_amr_data_with_accession)
}

#' annotate_amr_data
#'
#' convert amr dataframe to long format and join to NCBI metadata
#' @param amr_data acquired_amr_data obtained using the ghruR::get_data_for_country(country_value = "<COUNTRY>", type_value = "AMR <SPECIES>", AMR_type = "acquired")
#' @return annonated_amr_data dataframe with amr data converted to long format and annotated with NCBI metadata
annotate_amr_data <- function(amr_data){
  long_amr_data <- wide_amr_data_to_long(amr_data)
  long_amr_data <- extract_nucleotide_accession(long_amr_data)

  # get NCBI metadata and merge allele and gene_family
  ncbi_metadata <- memoised_get_ncbi_amr_metadata(c("refseq_nucleotide_accession", "allele", "gene_family", "product_name", "class","subclass")) %>%
    dplyr::mutate(gene = dplyr::coalesce(allele, gene_family,)) %>%
    select(-c(allele)) %>%
    drop_na(gene) %>%
    drop_na(refseq_nucleotide_accession)

  # annotate amr data with NCBI metadata
  annotated_amr_data <- long_amr_data %>%
    left_join(ncbi_metadata, by = "refseq_nucleotide_accession")
  return(annotated_amr_data)
}


#' wide_point_amr_data_to_long
#'
#' function to get convert point amr data into long format
#' @param amr_data point amr data obtained using the ghruR::get_data_for_country(country_value = "<COUNTRY>", type_value = "AMR <SPECIES>", AMR_type = "point")
#' @return long_point_amr_data a data frame with the point mutation data converted to long format for later filtering
wide_point_amr_data_to_long <- function(point_amr_data){
  long_point_amr_data <- point_amr_data %>%
    tidyr::pivot_longer(-`Sample id`, names_to = 'metric', values_to = 'value') %>%
    tidyr::extract(col = 'metric', into = c('gene', 'metric'), regex = '^([^\\.]+)\\.(.+)', remove = TRUE)
  return(long_point_amr_data)
}

#' get_annotated_point_amr_data
#'
#' convert amr dataframe to long format and join to NCBI metadata
#' @param amr_data point amr data obtained using the ghruR::get_data_for_country(country_value = "<COUNTRY>", type_value = "AMR <SPECIES>", AMR_type = "point")
#' @param species one of campylobacter, enterococcus_faecalis, enterococcus_faecium, escherichia_coli, helicobacter_pylori,
#' klebsiella, mycobacterium_tuberculosis, neisseria_gonorrhoeae, salmonella
#' @param annotation_type Either mutations (all mutations in genes that have fully assembled), incomplete (all genes that are not fully assembled)
#' @return annonated_amr_data dataframe with amr data converted to long format and annotated with NCBI metadata
get_annotated_point_amr_data <- function(point_amr_data, species, annotation_type){
  if (annotation_type != "mutations" && annotation_type != "incomplete") stop("'annotation_type' must be 'mutations' or 'incomplete'")
  long_point_amr_data <- wide_point_amr_data_to_long(point_amr_data)
  # get just assembled metric
  point_assembled_metrics <- long_point_amr_data %>%
    dplyr::filter(metric == 'assembled') %>%
    dplyr::rename(assembled = value) %>%
    dplyr::select(-metric)

  # get point mutation metadata
  point_mutation_metadata <- get_point_mutation_metadata(species)

  if (annotation_type == 'mutations'){
    # find those that are assembled fully
    fully_assembled_point_genes <- point_assembled_metrics  %>%
      dplyr::filter(stringr::str_starts(assembled, "yes")) %>%
      dplyr::select(-assembled)

    # link fully assembled genes to mutations
    point_mutations <- long_point_amr_data %>%
      dplyr::filter(metric != 'assembled')

    # find those samples with point mutations in the fully assembled genes
    fully_assembled_point_mutations <- fully_assembled_point_genes %>%
      dplyr::inner_join(point_mutations, by = c('Sample id', 'gene')) %>%
      dplyr::rename(mutation = metric, mutated = value) %>%
      dplyr::filter(mutated == "yes") %>%
      select(-mutated) %>%
      tidyr::unite(mutation, gene, mutation)

    # merge with point mutation metadata
    annotated_point_mutations <- fully_assembled_point_mutations %>%
      dplyr::inner_join(point_mutation_metadata, by = "mutation") %>%
      dplyr::rename(determinant = mutation)
  } else {
    # filter out incomplete genes
    incomplete_point_genes <- point_assembled_metrics  %>%
      filter(!stringr::str_starts(assembled, "yes"))

    annotated_point_mutations <- point_mutation_metadata %>%
      dplyr::mutate(gene = stringr::str_replace(mutation, "_.+$", "")) %>%
      dplyr::select(gene, resistance) %>%
      dplyr::distinct(gene, resistance)

    # merge incomplete genes with metadata to find association of gene with antibiotic
    annotated_point_mutations  <- incomplete_point_genes %>%
      dplyr::inner_join(annotated_point_mutations, by = "gene") %>%
      dplyr::mutate(assembled = stringr::str_replace(assembled, "no", "absent")) %>%
      tidyr::unite(determinant, gene, assembled, sep = "-")
  }

  return(annotated_point_mutations)

}


#' clean_ariba_cluster_names
#'
#' get cleaner ariba column names based on genes found within the sequences
#' @param ariba_cluster_and_gene_names A dataframe with Ariba headers (column name ariba_cluster_name) and gene_names
#' @param  max_header_names The maximum number of genes names in a column label before a common prefix is used instead
#' @return ariba_cluster_to_gene_name A dataframe with the names to replace the Ariba column names with
clean_ariba_cluster_names <- function(ariba_cluster_and_gene_names, max_header_names = NULL){
  # define function to get common gene pattern
  get_common_gene_prefix <- function(gene_names, max_names = NULL){
    if (is.null(max_names) || length(gene_names) <= max_names){
      return(paste(gene_names, collapse = ";"))
    } else {
      common_patterns <- CommonPatt(gene_names)
      longest_prefix <- common_patterns %>% arrange(desc(Percent_str), desc(Length)) %>% head(1) %>% pull(Pattern)
      return(str_replace(longest_prefix, "-$", ""))
    }
  }

  # add a common_gene_prefix
  ariba_cluster_to_gene_name <- ariba_cluster_and_gene_names %>%
    mutate(common_gene_prefix = map_chr(gene_names, get_common_gene_prefix, max_names = max_header_names)) %>%
    select(-gene_names) %>%
    na_if("NA")
  return(ariba_cluster_to_gene_name)
}

#' replace_ariba_cluster_names
#'
#' rename amr headers, converting ariba cluster to common prefix
#' @param annotated_amr_data acquired_amr_data obtained using the ghruR::get_data_for_country(country_value = "<COUNTRY>", type_value = "AMR <SPECIES>", AMR_type = "acquired") and annotated using the   annotate_amr_data function
#' @param  max_number_genes The maximum number of genes names in a column label before a common prefix is taken
#' @return final_long_amr_data A long format ready for filtering with columns amr_gene_column, refseq_nucleotide_accession, assembled, pct_id, ctg_cov
replace_ariba_cluster_names <- function(annotated_amr_data, max_number_genes = 3){
  # combine all genes for ariba clusters
  combined_cluster_genes <- annotated_amr_data %>%
    drop_na(refseq_nucleotide_accession) %>%
    group_by(ariba_cluster) %>%
    distinct(ariba_cluster, gene) %>%
    drop_na(gene) %>%
    summarise(gene_names = list(gene))

  ariba_cluster_to_gene_name <- clean_ariba_cluster_names(combined_cluster_genes, max_header_names = max_number_genes)

  # join to long amr data and coalesce
  final_long_amr_data <- annotated_amr_data %>%
    left_join(ariba_cluster_to_gene_name, by = "ariba_cluster") %>%
    mutate(amr_gene_column = coalesce(common_gene_prefix, ariba_cluster)) %>%
    select(`Sample id`, amr_gene_column, gene, assembled, pct_id, ctg_cov)

}
