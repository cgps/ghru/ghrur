% Generated by roxygen2: do not edit by hand
% Please edit documentation in R/microreact_data_manipulation_functions.R
\name{colour_dataframe}
\alias{colour_dataframe}
\title{colour_dataframe}
\usage{
colour_dataframe(
  data,
  id_column = "id",
  present_colour = "#ff6b6b",
  absent_colour = "#efefef",
  absence_characters = "-"
)
}
\arguments{
\item{data}{a dataframe with an id column and other columns to be coloured}

\item{id_column}{name of the id column}

\item{present_colour}{the html colour to use when a gene is present}

\item{absence_characters}{the character to use when a gene is absent}

\item{absent_color}{the html colour to use when a gene is absent}
}
\value{
coloured_data
}
\description{
A function that will take a data frame that has only an id column and all other columns should have a colour added for microreact
}
