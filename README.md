# ghruR

This package contains useful R functions for analysing GHRU data

See the [HTML docs](https://glcdn.githack.com/cgps/ghru/ghrur/raw/master/docs/index.html) for documentation of the functions

See these vignettes for examples
[basic principals](https://glcdn.githack.com/cgps/ghru/ghrur/raw/master/vignettes/analysing_ghru_data.html)
[making microreact data](https://glcdn.githack.com/cgps/ghru/ghrur/raw/master/vignettes/making_microreact_compatible_data.html)

## Installation
Install from the source from within the R console or the Console in RStudio using (R>= 3.2.0)
* First download dependencies
    ```
    install.packages(c("googledrive", "googlesheets4", "dplyr", "tidyr", "readr", "stringr", "purrr", "anytime", "magrittr", "ape", "tibble", "GrpString", "ggplot2", "ggeasy", "ggthemes", "RColorBrewer", "ggpubr", "cowplot", "ggupset", "epiR", "forcats", "memoise"",
  "collections"))
    ```

Choose one of the following options
1. Directly from the latest source code on gitlab (master branch)
    ```
    devtools::install_git("https://gitlab.com/cgps/ghru/ghrur.git")
    ```
2. Install a specific version (0.3.2 in this example)
    ```
    install.packages("https://gitlab.com/cgps/ghru/ghrur/raw/master/ghruR_0.3.2.tar.gz",repos=NULL, method="libcurl")
    ```

## Testing
To run the tests in the [tests](tests/testthat) directory use the following command
```
devtools::test()
```

## Make the docs html page
```
pkgdown::build_site()
```

## Make the vignette html page
If you have added new examples to the vignettes, use the following command to make a new html file
```
rmarkdown::render('vignettes/analysing_ghru_data.Rmd')
rmarkdown::render('vignettes/making_microreact_compatible_data.Rmd')
```

## Update documents
To generate docs for the package run 
```
devtools::document()
```

## Building the source
To build the package use the command
```
 devtools::build(path='.')
```

## Recommended development cycle
1. Edit code
2. Run tests
    ```
    devtools::test()
    ```
3. Build vignettes, docs and source package
    ```
    for (file in list.files('vignettes', pattern = '*.Rmd', full.names = TRUE)) { rmarkdown::render(file) }
    devtools::document()
    pkgdown::build_site(lazy = TRUE)
    devtools::build(path='.')
    ```
4. Commit changes to git and push
    ```
    git add .
    git commit -m 'Message about changes'
    git push
    ```


